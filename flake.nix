{
  inputs = {
      nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
      utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, utils }: (utils.lib.eachDefaultSystem (system:
    let
      pkgs = nixpkgs.legacyPackages."${system}";
    in {
        devShell = pkgs.mkShell {
          venvDir = "./venv";
          buildInputs = with pkgs; [
            python311Packages.venvShellHook
            python311Packages.flake8

            ffmpeg_6-full
            pre-commit
          ];
          postShellHook = ''
             export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:${pkgs.lib.makeLibraryPath [
               pkgs.stdenv.cc.cc
             ]}
          '';
        };
      }
  ));
}
