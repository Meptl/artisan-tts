import os

from bs4 import BeautifulSoup
import requests

outdir = "output"

r = requests.get("https://genshin-impact.fandom.com/wiki/Paimon/Companion")
soup = BeautifulSoup(r.content, 'html.parser')

# find all span elements with class "audio-button"
spans = soup.find_all('span', {'class' : 'audio-button'})

# for each span, find the child <a> tag and grab the href
# also grab the innerHTML of the parent
i = 0
for span in spans:
    atag = span.find('a')
    href = atag['href']
    file_name = atag['title'].replace(' ', '-')

    response = requests.get(href)
    with open(outdir + "/" + file_name, 'wb') as file:
        file.write(response.content)

    # Drop the <a>, a spacer, and "Paimon: "
    transcript = span.parent.contents[3:]
    line = ""
    for el in transcript:
        # Sometimes we get <a> tags so get_text()
        line += el.get_text()

    file_name_txt = os.path.splitext(file_name)[0] + ".txt"
    with open(outdir + "/" + file_name_txt , 'w') as file:
        file.write(line)
