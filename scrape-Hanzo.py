import os

from bs4 import BeautifulSoup
import requests

outdir = "output2"

r = requests.get("https://overwatch.fandom.com/wiki/Hanzo/Quotes")
soup = BeautifulSoup(r.content, 'html.parser')

audios = soup.find_all('audio', {'class' : 'ext-audiobutton'})

for a in audios:
    if a.parent.parent.name == 'p':
        # Sometimes there are multiple voicelines per row.
        # I don't want to deal with the conversational dialogue
        continue
    href = a.source["src"]
    file_name = href.split('/')[-3]
    print(file_name)

    # response = requests.get(href)
    # with open(outdir + "/" + file_name, 'wb') as file:
    #     file.write(response.content)

    transcript = a.parent.parent.find_previous_sibling("td").contents
    line = ""
    for el in transcript:
        # Sometimes we get <a> tags so get_text()
        line += el.get_text()

    line = line.strip().replace('\n', ' ')

    print(line)
    file_name_txt = os.path.splitext(file_name)[0] + ".txt"
    with open(outdir + "/" + file_name_txt , 'w') as file:
        file.write(line)
