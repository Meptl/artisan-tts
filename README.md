# artisan-tts

A hacky script that simply concats voice samples/voice lines.
```
USAGE: python main.py AUDIO_FOLDER
```
where AUDIO_FOLDER is a folder containing ogg files and a matching txt files.

The script allows the user to select files based on the txt file contents,
then parses the audio file using OpenAI whisper transcription.
Finally the user gets to select which segments of the audio file to use.
