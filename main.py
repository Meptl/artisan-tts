import os
import sys

from fzf_wrapper import prompt

if len(sys.argv) != 2:
    print("USAGE: python main.py AUDIO_FOLDER")
    sys.exit(1)

audio_folder = sys.argv[-1]


def generate_file_dict(directory):
   file_dict = {}
   for filename in os.listdir(directory):
       if not filename.endswith('.txt'):
           continue

       path = os.path.join(directory, filename)
       if os.path.isfile(path):
           with open(path, 'r') as file:
               file_dict[path] = file.read()
   return file_dict


def find_index_in_split(words, index):
   count = 0
   for i, word in enumerate(words.split()):
       if index <= count + len(word):
           return i
       count += len(word) + 1 # +1 for the space (Assumes single space
   return -1 # return -1 if index is out of range

def getch():
    import termios
    import sys, tty
    def _getch():
        fd = sys.stdin.fileno()
        old_settings = termios.tcgetattr(fd)
        try:
            tty.setraw(fd)
            ch = sys.stdin.read(1)
        finally:
            termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
        return ch
    return _getch()

data = generate_file_dict(audio_folder)

done = False
final_audio = None
while not done:
    start_time = None
    end_time = None

    choice = prompt(data.values(), "--exact --print-query")

    if len(choice) <= 1:
        # The user didn't give a prompt or quit out.
        done = True
        continue

    final_prompt = choice[0]
    selected_entry = choice[1]

    # Find the audio file name from the selected_entry
    file = ''
    for f, content in data.items():
        if content == selected_entry:
            file = f
    audio_file = os.path.splitext(file)[0] + ".ogg"

    # Perform transcription on the audio file
    import whisper_timestamped as whisper
    audio = whisper.load_audio(audio_file)
    model = whisper.load_model("medium", device="cpu")
    result = whisper.transcribe(model, audio, language="en", detect_disfluencies=True)
    transcription = result['text'].strip()
    from pprint import pprint
    pprint(result)
    words = transcription.split()

    start_word = None
    end_word = None
    while True:
        print("Original: ", selected_entry)
        print("Transcription: ", transcription)
        new_prompt = input("Please supply a FROM and TO word (inclusive) separated by a space: ")
        prompt_args = new_prompt.strip().split()

        if len(prompt_args) < 1 or len(prompt_args) > 2:
            print("Invalid")
            continue

        from_expr = prompt_args[0]
        from_idx = result['text'].find(from_expr)
        if from_idx == -1:
            print("Failed to find FROM word")
            continue
        start_word = find_index_in_split(transcription, from_idx)

        if len(prompt_args) == 2:
            to_expr = prompt_args[1]
            to_idx = transcription.find(to_expr, from_idx)
            if to_idx == -1:
                print("Failed to find TO word")
                continue
            # to_idx_end = result['text'].find(' ', to_idx)
            # if to_idx_end == -1:
            #     to_idx_end = len(transcription)
            # to_idx = to_idx_end
            end_word = find_index_in_split(transcription, to_idx)
        break

    print(start_word)
    print(end_word)

    # Find the start and stop time of chosen words
    words = [words for words in [seg["words"] for seg in result["segments"]]]
    words = [word for sublist in words for word in sublist]
    words = [x for x in words if x["text"] != "[*]"]
    print(words)
    if start_word == 0:
        start_time = 0.0
    else:
        # Half way between the end of last word and the start of this word.
        start_time = words[start_word - 1]["end"] + (words[start_word]["start"] - words[start_word - 1]["end"]) / 2.0

    if end_word:
        if end_word == len(words) - 1:
            end_time = result["segments"][-1]["end"]
        else:
            end_time = words[end_word]["end"]
    else:
        end_time = words[start_word]["end"]
    print(start_time)
    print(end_time)

    # Create the output wav.
    from pydub import AudioSegment
    audio = AudioSegment.from_ogg(audio_file)
    segment = audio[start_time*1000:end_time*1000]

    buffer = 0.0

    from pydub.playback import play
    if final_audio is None:
        play(segment - 10)

    accept = False
    accept_final = True
    if final_audio is None:
        print(f'q to reject. Any other key to accept.')
        user_char = getch()
        if user_char == 'q':
            accept_final = False
    while not final_audio is None and not accept:
        audio_buffer = AudioSegment.silent(duration=buffer * 1000)
        preview_audio = final_audio + audio_buffer + segment

        play(preview_audio - 10)

        print(f'Current buffer size: {buffer}sec. Use jJ/kK to raise or lower. q to reject. Any other key to accept.')
        user_char = getch()
        if user_char == 'j':
            buffer -= 0.1
            if buffer <= 0.0:
                buffer = 0.0
        elif user_char == 'J':
            buffer -= 0.5
            if buffer <= 0.0:
                buffer = 0.0
        elif user_char == 'k':
            buffer += 0.1
        elif user_char == 'K':
            buffer += 0.5
        elif user_char == 'q':
            accept_final = False
            accept = True
        else:
            accept = True


    if accept_final:
        if final_audio is None:
            final_audio = segment
        else:
            final_audio = final_audio + segment

if not final_audio is None:
    final_audio.export("output.mp3", format="mp3")
    print("Created output.mp3")
